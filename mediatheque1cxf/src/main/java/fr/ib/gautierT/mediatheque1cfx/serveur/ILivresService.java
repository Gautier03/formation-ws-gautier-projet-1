package fr.ib.gautierT.mediatheque1cfx.serveur;

import java.util.Date;

import javax.jws.WebService;

//mettre aussi le WebService
@WebService
public interface ILivresService {
	public String getInfos();
	public boolean estEmpruntable(int id);
	public Date getRetour(int id);
	public Livre getLivreDuMois();
	public Livre[] getLivresDeLannee();
}
