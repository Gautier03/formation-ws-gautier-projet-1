package fr.ib.gautierT.mediatheque1cfx.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import fr.ib.gautierT.mediatheque1cfx.serveur.ILivresService;

public class ClientMain {
	public static void main(String[] args) {
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setAddress("http://localhost:9001/livres");
		factory.setServiceClass(ILivresService.class);
		
		//element déprécié : pas conseillé de l'utiliser car ancien mais marche quand même
		factory.getOutInterceptors().add(new LoggingOutInterceptor());
		factory.getInInterceptors().add( new LoggingInInterceptor());
		
		// creation d'un objet selon l'interface et on l'utilise
		//On s'adresse ici au serveur avant de reçevoir la réponse
		ILivresService livresServices = factory.create(ILivresService.class);
		System.out.println(livresServices.getInfos());
		System.out.println("Livre 4 empruntable : "+livresServices.estEmpruntable(4));
		
		try {
		System.out.println("Livre -3 empruntable : "+livresServices.estEmpruntable(-3));
		} catch(Exception ex) {
			System.out.println("Exception :" +ex.getMessage());
		}
		
		System.out.println("Retour du livre 4 : "+livresServices.getRetour(4));
		
		System.out.println("Le livre du mois est : "+livresServices.getLivreDuMois());
		
		System.out.println("Le livre du mois de Mars est :" + livresServices.getLivresDeLannee()[2].getTitre());
	}

}
