package fr.ib.gautierT.mediatheque1cfx.serveur;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.jws.WebService;

@WebService
public class LivresService implements ILivresService {
	
	public String getInfos() {
		return "L'entrée se trouve ur la droite";
	}

	public boolean estEmpruntable(int id) {
		if (id<1)
			throw new IllegalArgumentException("id doit être supérieur à 1");
		return false;
	}

	public Date getRetour(int id) {
		LocalDate d = LocalDate.now();
		d = d.plusDays(10);
		return Date.from(d.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	public Livre getLivreDuMois() {
		Livre l1 = new Livre ("La plus secrete...", "M. M. Sarr", 2021);
		DataSource dataSource = new FileDataSource("La-plus-secrete-memoire-des-hommes.jpg");
		DataHandler dataHandler = new DataHandler(dataSource);
		l1.setImage(dataHandler);
		return l1;
	}

	public Livre[] getLivresDeLannee() {
		Livre[] livres = new Livre[12];
		for (int i=0 ; i<12 ; i++) {
			livres[i] = getLivreDuMois();
		}
		return livres;
	}
	
	

}
