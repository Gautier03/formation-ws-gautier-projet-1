package fr.ib.gautierT;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {
	@RequestMapping("/health")
	public String gethealth() {
		return "OK";
	}

}
