package fr.ib.gautierT;



public class Cd {
	
	private String titre;
	private String artiste;
	private int annee;
	
	
	public Cd(String t, String a, int ann) {
		titre = t;
		artiste = a;
		annee = ann;
	}


	public Cd() {
		this(null, null, 0);
	}


	public String getTitre() {
		return titre;
	}


	public void setTitre(String titre) {
		this.titre = titre;
	}


	public String getArtiste() {
		return artiste;
	}


	public void setArtiste(String artiste) {
		this.artiste = artiste;
	}


	public int getAnnee() {
		return annee;
	}


	public void setAnnee(int annee) {
		this.annee = annee;
	}


	@Override
	public String toString() {
		return titre + " de " + artiste + " sortie en " + annee;
	}
	
	
	
	
	
	

}
