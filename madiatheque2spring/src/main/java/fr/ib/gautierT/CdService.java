package fr.ib.gautierT;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class CdService {
	private List<Cd> cds;

	public CdService() {
		cds = new ArrayList<Cd>();
		
	}
	
	public int getNombreDeCd() {
		return cds.size();
	}
	
	public void ajouteCd(Cd cd) {
		cds.add(cd);
	}

	public List<Cd> getCds() {
		return cds;
	}
	
	public Cd getCd(int n) {
		return cds.get(n);
	}
	
	public void changeTitre(int n, String titre) {
		cds.get(n).setTitre(titre);
	}
	
}
