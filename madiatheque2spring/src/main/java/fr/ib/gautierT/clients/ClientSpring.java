package fr.ib.gautierT.clients;


import java.util.ArrayList;
import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import fr.ib.gautierT.Cd;

public class ClientSpring {
	
	public static void main(String[] args) {
	

	try {
		System.out.println("Client Java");
		
		RestTemplate rt = new RestTemplate();
		//getForObject convertie tout en chaine de caractère
		String health = rt.getForObject("http://localhost:9002/health", String.class);	
		System.out.println("Health" + health);
		
		Integer nbCd = rt.getForObject("http://localhost:9002/cd/nombre", Integer.class);
				
		System.out.println("il y a " +nbCd+ " CD à la médiathèque");
		
		//on crée un nouveau cd
		Cd cd1 = new Cd("Abbey Road", "The Beattles", 1966);
		rt.postForObject("http://localhost:9002/cd", cd1, Void.class); //void : méthode qui n'attend pas de resultat
		
		
		//solution 1 pour afficher la liste des cd
		//ParameterizedTypeReference<List<Cd>> ref = new ParameterizedTypeReference<List<Cd>>() {};
		//ResponseEntity<List<Cd>> cdsEntity = rt.exchange("http://localhost:9002/cd", HttpMethod.GET, null, ref);
		//List<Cd>cds = cdsEntity.getBody();
		//for (Cd cd:cds) {
			//System.out.println(" - "+cd);
		//}
		
		//solution 2
		Cd[] cds = rt.getForObject("http://localhost:9002/cd", Cd[].class);
		System.out.println("Tous les CD : ");
		for (Cd cd:cds) {
			System.out.println(" - "+cd);
		}
		
		Cd cd0 = rt.getForObject("http://localhost:9002/cd/0", Cd.class);
		System.out.println("premier CD : "+cd0);
		
		//Modifier le titre du cd
		
		rt.put("http://localhost:9002/cd/0/titre", "help!", String.class);
		
	
	} catch (Exception ex) {
		System.err.println("Erreur : "+ex);
		}
	}
}

