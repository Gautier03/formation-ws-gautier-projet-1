package fr.ib.gautierT.clients;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class ClientJavaMain {

	public static void main(String[] args) {
		try {
			System.out.println("Client Java");
			//permet de lire à partir d'une url
			URL url = new URL("http://localhost:9002/health");
			URLConnection conn =  url.openConnection();
			//je veux creer un flux en entrée pour lire ce que renvoi un serveur
			InputStream is = conn.getInputStream();
			//BufferedReader permet de lire ligne par ligne
			BufferedReader br = new BufferedReader((new InputStreamReader(is)));
			String reponse = br.readLine();
			System.out.println("Health : "+reponse);
			br.close();
			
		
		
		} catch (Exception ex) {
			System.err.println("Erreur"+ex);
		}
		

	}

}
