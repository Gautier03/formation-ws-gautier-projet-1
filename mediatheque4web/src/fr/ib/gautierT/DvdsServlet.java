package fr.ib.gautierT;

import java.io.IOException;
import java.io.Writer;
import java.time.LocalTime;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.ib.gautierT.madiatheque3EJB.Dvd;
import fr.ib.gautierT.madiatheque3EJB.IDvdDAO;
import fr.ib.gautierT.madiatheque3EJB.IDvdtheque;

@WebServlet("/dvds")
public class DvdsServlet extends HttpServlet  {

	private static final long serialVersionUID = 1L;
	
	private IDvdtheque dvdtheque;

	private IDvdDAO dvdDAO;


	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
	
	Writer out = resp.getWriter();
	out.write("<!DOCTYPE html><html><body>");
	out.write("<h1>DVDthèque</h1>");
	out.write("<p>"+dvdtheque.getInfos()+"</p>");
	out.write("<p>Ouvert à 9h ?"+dvdtheque.ouvertA(LocalTime.of(9,  0))+"</p>");
	out.write("<p>Quand ? "+dvdtheque.getDerniereInterrogation()+"</p>");
	
	out.write("<table>");
	out.write("<tr><th>Titre</th><th>Artiste</th></tr>");
	for(Dvd dvd : dvdDAO.lireTous()) {
		out.write("<tr><td>"+dvd.getTitre()+"</td>"+
					"<td>"+dvd.getAnnee()+"</td></tr>");
	}
	
	out.write("</table>");
	out.write("</body></html>");
	out.close();
	}

	@EJB(lookup="ejb:/mediatheque3EJB/Dvds!fr.ib.gautierT.madiatheque3EJB.IDvdtheque")
	public void setDvdtheque(IDvdtheque dvdtheque) {
		this.dvdtheque = dvdtheque;
	}
	
	@EJB(lookup="ejb:/mediatheque3EJB/DvdDAO!fr.ib.gautierT.madiatheque3EJB.IDvdDAO")
	public void setDvdtheque(IDvdDAO dvdDAO) {
		this.dvdDAO = dvdDAO;
	}

}
