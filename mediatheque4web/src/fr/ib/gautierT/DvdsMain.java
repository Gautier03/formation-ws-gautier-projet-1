package fr.ib.gautierT;

import java.time.LocalTime;

import javax.naming.Context;
import javax.naming.InitialContext;

import fr.ib.gautierT.madiatheque3EJB.Dvd;
import fr.ib.gautierT.madiatheque3EJB.IDvdDAO;
import fr.ib.gautierT.madiatheque3EJB.IDvdtheque;

public class DvdsMain {

	public static void main(String[] args) {
		
		System.out.println("Dvd : client lourd");
		try {
		Context context = new InitialContext();
		
		IDvdtheque dvdtheque = (IDvdtheque) context.lookup(
				"ejb:/mediatheque3EJB/Dvds!fr.ib.gautierT.madiatheque3EJB.IDvdtheque");
		System.out.println("Information : "+dvdtheque.getInfos());
		System.out.println("Ouvert à 17h30 ? "+dvdtheque.ouvertA(LocalTime.of(17,  30)));
		
		IDvdDAO dvdDAO = (IDvdDAO) context.lookup("ejb:/mediatheque3EJB/DvdDAO!fr.ib.gautierT.madiatheque3EJB.IDvdDAO");
		dvdDAO.ajouter(new Dvd("Casino Royal", 2006));
		dvdDAO.ajouter(new Dvd("Quantum of Solace", 2008));
		dvdDAO.ajouter(new Dvd("Skyfall", 2012));
		dvdDAO.ajouter(new Dvd("Spectre", 2015));
		dvdDAO.ajouter(new Dvd("No time to die",2021));
		System.out.println("Il y en a maintenant : "+ dvdDAO.getNombre());
		
		
	
		
		
		
		
		
		context.close();
		
		} catch (Exception ex) {
			System.err.println("ex");
			ex.printStackTrace();
		}
		
		
	}

}
