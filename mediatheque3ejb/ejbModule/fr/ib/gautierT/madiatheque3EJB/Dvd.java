package fr.ib.gautierT.madiatheque3EJB;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Dvd implements Serializable{
	private int id;
	private String titre;
	private int annee;
	
	
	public Dvd(String t, int a) {
		titre = t;
		annee = a;
	}


	public Dvd() {
		this(null, 1900);
	}

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	@Column(length=50, nullable=false, unique=false)
	public String getTitre() {
		return titre;
	}


	public void setTitre(String titre) {
		this.titre = titre;
	}

	@Column(length=50, nullable=false)
	public int getAnnee() {
		return annee;
	}


	public void setAnnee(int annee) {
		this.annee = annee;
	}


	@Override
	public String toString() {
		return id + " : " + titre + " sortie en : " + annee;
	}
	
	
	
	

}
