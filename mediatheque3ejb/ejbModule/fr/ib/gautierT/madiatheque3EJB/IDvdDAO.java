package fr.ib.gautierT.madiatheque3EJB;

import java.util.List;

import javax.ejb.Remote;

@Remote
public interface IDvdDAO {
	public int getNombre();
	public void ajouter(Dvd dvd);
	public Dvd lire(int id);
	public List<Dvd> lireTous();
	


}
