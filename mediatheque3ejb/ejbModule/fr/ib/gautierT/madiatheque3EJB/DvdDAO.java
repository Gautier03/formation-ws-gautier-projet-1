package fr.ib.gautierT.madiatheque3EJB;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless(name="DvdDAO", description="Stockage JPA des DVD")
public class DvdDAO implements IDvdDAO{
	
	private EntityManager em;
	
	public int getNombre() {
		return em.createQuery("select count(*) from Dvd", Long.class).getSingleResult().intValue(); 
	}

	@Override
	public void ajouter(Dvd dvd) {
		//permet d'ajouter un dvd
		em.persist(dvd);
	}

	@Override
	public Dvd lire(int id) {
		return em.find(Dvd.class, id);
	}

	@Override
	public List<Dvd> lireTous() {
		return em.createQuery("from Dvd order by annee", Dvd.class).getResultList();
	}
	
	@PersistenceContext(unitName = "DvdPU")
	public void setEm(EntityManager em) {
		this.em = em;
	}

}