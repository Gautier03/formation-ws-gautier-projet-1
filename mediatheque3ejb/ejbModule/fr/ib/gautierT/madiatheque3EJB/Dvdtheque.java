package fr.ib.gautierT.madiatheque3EJB;

import java.time.LocalTime;

import javax.ejb.EJB;
import javax.ejb.Stateless;



//designe un bean de session
//statefull : la valeur à l'interieur de l'ejb sont important
//stateless : les valeurs ne sont pas sauvegardée
@Stateless(name="Dvds", description="Opérationpour a DVDThèque")
public class Dvdtheque implements IDvdtheque{
	private LocalTime derniereInterrogation;
	private IDvdDAO dvdDAO;
	
	public String getInfos() {
		return "Nouvelle DVDthèque, ouverte de 10h à 18h"+
	" et il y a "+dvdDAO.getNombre()+" DVD.";
	}
	
	public boolean ouvertA(LocalTime t) {
		derniereInterrogation = t;
		return t.isAfter(LocalTime.of(10,  0)) && t.isBefore(LocalTime.of(18, 0));
		
	}
	
	public LocalTime getDerniereInterrogation() {
		return derniereInterrogation;
	}
	
	//@EJB(lookup="ejb:/mediatheque3EJB/DvdDAO!fr.ib.gautierT.madiatheque3EJB.IDvdDAO")
	@EJB(name="DvdDAO")
	public void setDvdDAO(IDvdDAO dvdDAO) {
		this.dvdDAO = dvdDAO;
	}
	
	
	
}
