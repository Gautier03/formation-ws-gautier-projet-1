package fr.ib.gautierT.madiatheque3EJB;

import java.time.LocalTime;

import javax.ejb.Remote;

@Remote
public interface IDvdtheque {
	public String getInfos() ;
	public boolean ouvertA(LocalTime t);
	public LocalTime getDerniereInterrogation();

}
